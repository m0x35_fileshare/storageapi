IP=storageboard
dir=/home/m0x35/projects/fileshare-storageapi

scp build/libs/*jar $IP:${dir}/
ssh $IP "pkill -f storageapi"
ssh $IP "cd ${dir} && tmux new-window 'java -jar storageapi*.jar'"
