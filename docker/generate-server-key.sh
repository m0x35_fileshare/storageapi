#!/bin/sh

mkdir certs
cd certs
openssl genrsa -out ca.key 4096
openssl req -new -x509 -days 365 -key ca.key -out ca.crt
openssl genrsa -out nginx.key 1024
openssl req -new -key nginx.key -out nginx.csr -nodes
openssl x509 -req -days 365 -in nginx.csr -CA ca.crt -CAkey ca.key -set_serial 01 -out nginx.crt
#openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout nginx.key -out nginx.crt
#openssl dhparam -out dhparam.pem 2048
