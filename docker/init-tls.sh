#!/bin/sh

host=${2:-http://127.0.0.1:4567}

mkdir certs
cd certs
openssl genrsa -out nginx.key 1024
openssl req -new -key nginx.key -out nginx.csr -nodes

curl -f --data-binary @nginx.csr ${host}/sign-request/$1 > nginx.crt
