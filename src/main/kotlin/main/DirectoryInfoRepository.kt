package main

import main.dto.DirectoryInfo
import main.dto.FileInfo
import java.util.*

interface DirectoryInfoRepository {
    fun find(id: UUID): Optional<DirectoryInfo>
    fun findByPath(path: String): Optional<DirectoryInfo>
    fun findByParentDirId(directoryId: UUID): Collection<DirectoryInfo>
    fun addAll(items: Collection<DirectoryInfo>)
    fun add(entry: DirectoryInfo)
    fun removeByPath(directoryPath: String)
    fun clear()
}