package main

import main.dto.FileInfo
import java.util.*

interface FileInfoRepository {
    fun findByFilePath(filePath: String): Optional<FileInfo>
    fun findByDirId(directoryId: UUID): Collection<FileInfo>
    fun addAll(entries: Collection<FileInfo>): Collection<FileInfo>
    fun add(entry: FileInfo)
    fun removeByPath(filePath: String)
    fun clear()
}