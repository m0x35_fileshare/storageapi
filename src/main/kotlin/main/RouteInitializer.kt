package main

import spark.kotlin.Http;
import com.fasterxml.jackson.module.kotlin.*
import main.restcontrollers.Controller
import main.restcontrollers.RequestHandlerInvoker
import main.restcontrollers.RequestMethod
import main.restcontrollers.RestRequestHandler
import spark.kotlin.RouteHandler

class RouteInitializer(private val invoker: RequestHandlerInvoker) {
    private val controllers = ArrayList<Controller>()

    fun addController(c: Controller) = controllers.add(c)

    fun init(http: Http) {
        controllers.forEach{ controller ->
            controller.handlers().forEach { handler ->
                addHandler(http, controller.path(), handler)
            }
        }
    }

    private fun addHandler(http: Http, basePath: String, handler: RestRequestHandler) {
        when (handler.method()) {
            RequestMethod.GET -> http.get(basePath + handler.path()) {
                invoker.invoke(handler, request, response)
            }
            RequestMethod.POST -> http.post(basePath + handler.path()) {
                invoker.invoke(handler, request, response)
            }
        }
    }
}