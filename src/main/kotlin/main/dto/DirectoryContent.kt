package main.dto

import java.util.*

data class DirectoryContent (
        val upLevelDirId: UUID?,
        val entries: Collection<DirectoryEntry>
)
