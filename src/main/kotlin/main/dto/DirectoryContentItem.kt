package main.dto

import java.util.*

data class DirectoryContentItem(
        val id: UUID,
        val name: String,
        val isDir: Boolean,
        val size: Long,
        val sharedIds: Collection<UUID>
)