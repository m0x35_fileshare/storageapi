package main.dto

import java.util.*

data class DirectoryEntry(
        var id: UUID,
        var isDir: Boolean = false,
        var name: String = "",
        var size: Long = 0
)
