package main.dto

import java.util.*

data class FileId (
    var id: UUID? = null,
    var filePath: String = ""
)