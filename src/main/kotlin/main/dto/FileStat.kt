package main.dto

import java.util.*

data class FileStat (
    val id: UUID,
    val name: String,
    val isDir: Boolean,
    val size: Long
)