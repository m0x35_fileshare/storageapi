package main.logic

import main.dto.DirectoryContent
import java.util.*

interface DirectoryContentProvider {
    fun getContent(directoryId: UUID): Optional<DirectoryContent>
    fun getRootContent(): DirectoryContent
}