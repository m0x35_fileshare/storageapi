package main.logic.internal

import main.DirectoryInfoRepository
import main.FileInfoRepository
import main.dto.DirectoryContent
import main.dto.DirectoryEntry
import main.dto.DirectoryInfo
import main.logic.DirectoryContentProvider
import java.util.*
import kotlin.collections.ArrayList

class DbDirectoryContentProvider(private val directoryInfoRepository: DirectoryInfoRepository,
                                 private val fileInfoRepository: FileInfoRepository) : DirectoryContentProvider {

    override fun getContent(directoryId: UUID): Optional<DirectoryContent> {
        val optioanlDirInfo = directoryInfoRepository.find(directoryId)
        if (!optioanlDirInfo.isPresent)
            return Optional.empty()

        return getContent(optioanlDirInfo.get())
    }

    override fun getRootContent(): DirectoryContent {
        val optioanlDirInfo = directoryInfoRepository.findByPath("")
        return getContent(optioanlDirInfo.get()).get()
    }

    private fun getContent(dir: DirectoryInfo): Optional<DirectoryContent> {
        val directoryId = dir.id!!
        val directories = directoryInfoRepository.findByParentDirId(directoryId)
        val files = fileInfoRepository.findByDirId(directoryId)

        val entries = ArrayList<DirectoryEntry>(directories.size + files.size)

        directories.forEach { entries.add(DirectoryEntry(id = it.id!!, name = it.name, isDir = true)) }
        files.forEach { entries.add(DirectoryEntry(id = it.id!!, name = it.name, size = it.size, isDir = false)) }

        return Optional.of(DirectoryContent(upLevelDirId = dir.parentDir, entries = entries))
    }
}