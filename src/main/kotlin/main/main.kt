package main

import main.logic.internal.DbDirectoryContentProvider
import main.restcontrollers.Ls
import main.restcontrollers.ObjectMapperProvider
import main.restcontrollers.RequestHandlerInvoker
import main.restcontrollers.Status
import org.sql2o.Sql2o
import spark.kotlin.Http
import spark.kotlin.ignite

fun main(args: Array<String>) {
    val http: Http = ignite()

    var cfgPath = "./cfg.ini"
    if (args.isNotEmpty())
        cfgPath = args[0]
    val cfg = Config(cfgPath)

    val sql2oConnectionProvider = Sql2oConnectionProvider(provideSql2oDb(cfg))
    val requestHandlerInvoker = RequestHandlerInvoker(ObjectMapperProvider())
    val routeInitializer = RouteInitializer(requestHandlerInvoker)
    val sql2oDirectoryInfoRepository = Sql2oDirectoryInfoRepository(sql2oConnectionProvider)
    val sql2oFileInfoRepository = Sql2oFileInfoRepository(sql2oConnectionProvider)
    val directoryContentProvider = DbDirectoryContentProvider(sql2oDirectoryInfoRepository, sql2oFileInfoRepository)
    val lsController = Ls(directoryContentProvider)
    val statusController = Status()

    routeInitializer.addController(lsController)
    routeInitializer.addController(statusController)
    routeInitializer.init(http)
}

fun provideSql2oDb(cfg: Config): Sql2o {
    return Sql2o(cfg.getDbConnectionStr(), cfg.getDbUsername(), cfg.getDbPassword(), Sql2oQuirks())
}
