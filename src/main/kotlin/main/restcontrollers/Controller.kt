package main.restcontrollers

interface Controller {
    fun path(): String
    fun handlers(): Collection<RestRequestHandler>
}