package main.restcontrollers

import main.logic.DirectoryContentProvider
import spark.Request
import java.util.*

class Ls(private val dirContentProvider: DirectoryContentProvider) : Controller {
    override fun handlers() = arrayListOf(
        RootDirHandler(dirContentProvider),
        DirHandler(dirContentProvider)
    )

    override fun path() = "/ls"

    private class RootDirHandler(private val dirContentProvider: DirectoryContentProvider): RestRequestHandler {
        override fun method(): RequestMethod = RequestMethod.GET

        override fun handle(req: Request): RestResponse {
            return RestResponse(body = dirContentProvider.getRootContent())
        }

        override fun path() = ""
    }

    private class DirHandler(private val dirContentProvider: DirectoryContentProvider): RestRequestHandler {
        override fun handle(req: Request): RestResponse {
            val id = req.params(":id")
            val content = dirContentProvider.getContent(UUID.fromString(id))
            if (!content.isPresent)
                return RestResponse(statusCode = HttpStatus.NOT_FOUND, error = "No such directory id.")
            return RestResponse(body = content.get())
        }

        override fun method(): RequestMethod = RequestMethod.GET

        override fun path() = "/:id"
    }
}
