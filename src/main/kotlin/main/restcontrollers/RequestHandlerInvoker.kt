package main.restcontrollers

import spark.Request
import spark.Response
import java.io.StringWriter
import java.lang.Exception


class RequestHandlerInvoker(private val objectMapperProvider: ObjectMapperProvider) {

    private data class Error(val error: String)

    fun invoke(handler: RestRequestHandler, request: Request, response: Response): String {

        try {
            val restResponse = handler.handle(request)

            response.status(restResponse.statusCode.status)

            val body = if (restResponse.error.isEmpty()) restResponse.body else Error(restResponse.error)
            response.body(mapToJson(body))
        } catch (e: Exception) {
            response.status(HttpStatus.INTERNAL_SERVER_ERROR.status)
            response.body(mapToJson(Error(if (e.message != null) e.message!! else "Unknown error")))
            e.printStackTrace()
        }

        return response.body()
    }

    private fun mapToJson(obj: Any): String {
        if (obj is String)
            return obj

        val sw = StringWriter()
        objectMapperProvider.getObjectMapper().writeValue(sw, obj)
        return sw.toString()
    }

}