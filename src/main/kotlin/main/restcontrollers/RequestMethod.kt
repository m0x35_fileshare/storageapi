package main.restcontrollers

enum class RequestMethod {
    GET,
    POST
}