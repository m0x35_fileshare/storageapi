package main.restcontrollers

import spark.Request

interface RestRequestHandler {
    fun handle(req: Request): RestResponse
    fun path(): String
    fun method(): RequestMethod
}