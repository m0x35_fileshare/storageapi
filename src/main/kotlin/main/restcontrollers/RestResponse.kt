package main.restcontrollers


data class RestResponse(
    var statusCode: HttpStatus = HttpStatus.OK,
    var body: Any = "",
    var error: String = ""
)