package main.restcontrollers

import spark.Request


class Status : Controller{

    private class Handler : RestRequestHandler {
        private data class StatusResponse(var status: String)
        private val ok = StatusResponse("ok")
        override fun path(): String =  ""
        override fun method(): RequestMethod = RequestMethod.GET

        override fun handle(req: Request): RestResponse {
            return RestResponse(body = ok)
        }
    }

    override fun path(): String = "/status"
    override fun handlers(): Collection<RestRequestHandler> = arrayListOf(
        Handler()
    )
}